# Openshift tooling for OpenWRT CI/CD

This project hosts OCI container images for the OpenWRT
[SDK](https://openwrt.org/docs/guide-developer/obtain.firmware.sdk) and
[Imagebuilder](https://openwrt.org/docs/guide-user/additional-software/imagebuilder)
toolchains.

## Container registry

The [container registry](https://gitlab.inria.fr/midi-bidouille/openshift/container_registry)
contains three images:
1. **openshift/imagebuilder** to build firmware images
1. **openshift/sdk-feeds** to compile packages quickly using preloaded feeds
1. **openshift/sdk** to compile packages

Each of the images is available with several tags, one for each target architecture.


## Helm chart

This repository contains a Helm chart that creates Openshift BuildConfigs that can be
used to assemble these images and upload them to the registry. Uploading relies on a
push token in a Secret `inria-gitlab`, not supplied here.

Deploy the Helm chart in the [`openwrt`](https://console-openshift-console.apps.pleiade.bordeaux.inria.fr/k8s/cluster/projects/openwrt)
namespace on Pleaidès using

```sh
helm install -n openwrt openwrt-builders . -f values.yaml
```

Chart parameters in [`values.yaml`](values.yaml) specify which OpenWRT version to
install and which target architectures to support.


### Custom GitLab Runner in Helm Chart

To include a custom GitLab Runner in the Helm deployment, set `runner.deploy: true` in `values.yaml`. An existing secret containing a runner token can be used. 

If a new runner token has been generated, the chart can create the secret. Set the
base64 encoded secret in `runner.yaml.dec` and encrypt it with `helm secrets encrypt`
into `runner.yaml`. Deploy using

```sh
helm secrets install -n openwrt openwrt-builders . -f values.yaml -f runner.yaml
```
